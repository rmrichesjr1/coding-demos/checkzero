// checkzero.go

// This quick program checks whether a file or block device contains all zero
// data.  In case of non-zero data, information is printed.
//
// Syntax:
//
//     checkzero [input-file [output-file]]
//
// Omitted arguments use stdin and stdout.  Argument values of "-" use stdin
// and stdout.
//
// Serving suggestion to do multiple runs in parallel:
//
//     parallel -j3 checkzero ::: in{1,2,3} :::+ out{1,2,3}

package main

import (
  "fmt"
  "io"
  "log"
  "os"
  "time"
)

//////////////////////////////////////////////////////////////////////////////

// Constant:

// Print this many bytes per row:
const rowLength = 16

// Mask to detect out of row alignment:
const rowMask uint64 = 0xf

// NOTE: Make sure this is a multiple of rowLength:
//
// Will try to read this many bytes:
const attemptBlock = 1024 * 1024

// Line of hyphens to print for a block of all-zero rows:
const blockDelimiter = "--------------------------------"

//////////////////////////////////////////////////////////////////////////////

// Message printing utility functions:

// Set this to non-zero to enable debug printing:
const enableDebugPrint = 0
// const enableDebugPrint = 1

func printDebug(msg string) {
  if (0 == enableDebugPrint) {
    return
  }
  fmt.Printf( "DEBUG:     %s\n", msg)
}

func printInfo(msg string) {
  fmt.Printf( "     info: %s\n", msg)
}

func printWarning(msg string) {
  fmt.Printf( "  Warning: %s\n", msg)
}

// This function takes a string.
// This function does not return:
func printError(msg string) {
  fmt.Printf( "*** ERROR: %s\n", msg)
  os.Exit(1)
}

// This function takes an error.
// This function does not return:
func printFatal(err error) {
  log.Fatal(err)
  os.Exit(1)
}

// This function does not return:
func printUsageError(msg string) {
  fmt.Printf( "*** ERROR: %s\n\n", msg)
  printUsage()
  os.Exit(1)
}

// Print the usage message and return:
func printUsage() {
  fmt.Printf("usage: %s [input-file [output-file]]\n", os.Args[0])
}

//////////////////////////////////////////////////////////////////////////////

func getArgs() ( string, string, * os.File, * os.File) {
  // Default to use stdin and stdout.
  inputName := "(stdin)"
  outputName := "(stdout)"
  theInput  := os.Stdin
  theOutput := os.Stdout
  var err error = nil
  //
  leng := len(os.Args)
  if (3 < leng) {
    // Too many arguments; error out.
    printUsageError("Too many arguments.")
    // That function does not return.
  }
  if (2 < leng) {
    // Output filename specified.
    outputName = os.Args[2]
    if ("-" != outputName) {
      printDebug("Output file specified: " + outputName)
      theOutput, err = os.Create(outputName)
      if (nil != err) {
        printError("Failed to open output file: " + outputName)
        // That function does not return.
      }
    }
  }
  if (1 < leng) {
    // Input filename specified.
    inputName = os.Args[1]
    if ("-" != inputName) {
      printDebug("Input file specified: " + inputName)
      theInput, err = os.Open(inputName)
      if (nil != err) {
        printError("Failed to open input file: " + inputName)
        // That function does not return.
      }
    }
  }
  return inputName, outputName, theInput, theOutput
}

//////////////////////////////////////////////////////////////////////////////

func hasNonZero( theSlice [] byte) bool {
  leng := len(theSlice)
  for i := 0; leng > i; i++ {
    if (0 != theSlice[i]) {
      return(true)
    }
  }
  // If we get here, there were no non-zero bytes.
  return(false)
}

//////////////////////////////////////////////////////////////////////////////

// Produce a slice of strings 256 long, a lookup table for printing.
// Maximum string length is 3.

func makePrintTable() [] string {
  result := make( [] string, 256)

  // ASCII:

  // Control characters:
  result[0x00] = "NUL"
  result[0x01] = "^a"
  result[0x02] = "^b"
  result[0x03] = "^c"
  result[0x04] = "^d"
  result[0x05] = "^e"
  result[0x06] = "^f"
  result[0x07] = "^g"
  result[0x08] = "^h"
  result[0x09] = "^i"
  result[0x0a] = "^j"
  result[0x0b] = "^k"
  result[0x0c] = "^l"
  result[0x0d] = "^m"
  result[0x0e] = "^n"
  result[0x0f] = "^o"

  result[0x10] = "^p"
  result[0x11] = "^q"
  result[0x12] = "^r"
  result[0x13] = "^s"
  result[0x14] = "^t"
  result[0x15] = "^u"
  result[0x16] = "^v"
  result[0x17] = "^w"
  result[0x18] = "^x"
  result[0x19] = "^y"
  result[0x1a] = "^z"
  result[0x1b] = "ESC"
  result[0x1c] = "FS"
  result[0x1d] = "GS"
  result[0x1e] = "RS"
  result[0x1f] = "US"

  // Punctuation and digits:
  result[0x20] = " "
  result[0x21] = "!"
  result[0x22] = "\""
  result[0x23] = "#"
  result[0x24] = "$"
  result[0x25] = "%"
  result[0x26] = "&"
  result[0x27] = "'"
  result[0x28] = "("
  result[0x29] = ")"
  result[0x2a] = "*"
  result[0x2b] = "+"
  result[0x2c] = ","
  result[0x2d] = "-"
  result[0x2e] = "."
  result[0x2f] = "/"

  result[0x30] = "0"
  result[0x31] = "1"
  result[0x32] = "2"
  result[0x33] = "3"
  result[0x34] = "4"
  result[0x35] = "5"
  result[0x36] = "6"
  result[0x37] = "7"
  result[0x38] = "8"
  result[0x39] = "9"
  result[0x3a] = ":"
  result[0x3b] = ";"
  result[0x3c] = "<"
  result[0x3d] = "="
  result[0x3e] = ">"
  result[0x3f] = "?"

  // Uppercase letters:
  result[0x40] = "@"
  result[0x41] = "A"
  result[0x42] = "B"
  result[0x43] = "C"
  result[0x44] = "D"
  result[0x45] = "E"
  result[0x46] = "F"
  result[0x47] = "G"
  result[0x48] = "H"
  result[0x49] = "I"
  result[0x4a] = "J"
  result[0x4b] = "K"
  result[0x4c] = "L"
  result[0x4d] = "M"
  result[0x4e] = "N"
  result[0x4f] = "O"

  result[0x50] = "P"
  result[0x51] = "Q"
  result[0x52] = "R"
  result[0x53] = "S"
  result[0x54] = "T"
  result[0x55] = "U"
  result[0x56] = "V"
  result[0x57] = "W"
  result[0x58] = "X"
  result[0x59] = "Y"
  result[0x5a] = "Z"
  result[0x5b] = "["
  result[0x5c] = "\\"
  result[0x5d] = "]"
  result[0x5e] = "^"
  result[0x5f] = "_"

  // Lowercase letters:
  result[0x60] = "`"
  result[0x61] = "a"
  result[0x62] = "b"
  result[0x63] = "c"
  result[0x64] = "d"
  result[0x65] = "e"
  result[0x66] = "f"
  result[0x67] = "g"
  result[0x68] = "h"
  result[0x69] = "i"
  result[0x6a] = "j"
  result[0x6b] = "k"
  result[0x6c] = "l"
  result[0x6d] = "m"
  result[0x6e] = "n"
  result[0x6f] = "o"

  result[0x70] = "p"
  result[0x71] = "q"
  result[0x72] = "r"
  result[0x73] = "s"
  result[0x74] = "t"
  result[0x75] = "u"
  result[0x76] = "v"
  result[0x77] = "w"
  result[0x78] = "x"
  result[0x79] = "y"
  result[0x7a] = "z"
  result[0x7b] = "{"
  result[0x7c] = "|"
  result[0x7d] = "}"
  result[0x7e] = "~"
  result[0x7f] = "DEL"

  // Above ASCII:
  for i := 128; 256 > i; i++ {
    result[i] = "M-?"
  }

  return(result)
}

//////////////////////////////////////////////////////////////////////////////

// Conditionally print a line of hyphens.

func printTheHyphens( previousOffset uint64, newOffset uint64,
                      theOutput * os.File) {

  if (rowLength < (newOffset - previousOffset)) {
    fmt.Fprintln( theOutput, blockDelimiter)
  }
}

//////////////////////////////////////////////////////////////////////////////

func printRow( thisOffset uint64, theSlice [] byte, theOutput * os.File,
               printTable [] string) {
  leng := len(theSlice)
  fmt.Fprintf( theOutput, "%d, 0x%x\n", thisOffset, thisOffset)
  theHex := ""
  thePrinted := ""
  for i := 0; leng > i; i++ {
    theByte := theSlice[i]
    // Four characters per byte:
    theHex     += fmt.Sprintf( "  %02x", theByte)
    thePrinted += fmt.Sprintf( " %3s", printTable[theByte])
  }
  fmt.Fprintf( theOutput, "    %s\n", theHex)
  fmt.Fprintf( theOutput, "    %s\n", thePrinted)
}

//////////////////////////////////////////////////////////////////////////////

// Conditionally print a line of hyphens, print a row, and return the new
// previous offest.

func printHyphensAndRow( previousOffset uint64, newOffset uint64,
                         theOutput * os.File, thisSlice [] byte,
                         printTable [] string) uint64 {
  printTheHyphens( previousOffset, newOffset, theOutput)
  printRow( newOffset, thisSlice, theOutput, printTable)
  previousOffset = newOffset
  return(newOffset)
}

//////////////////////////////////////////////////////////////////////////////

func doWork( inputName string, outputName string,
             theInput * os.File, theOutput * os.File,
             printTable [] string) {
  printDebug("doWork starting:")
  // 
  // Initial state:
  var theOffset uint64 = 0
  rowsPrinted := 0
  theBuffer := make( [] byte, attemptBlock)
  startTime := time.Now()
  var previousOffset uint64 = 0
  //
  // Loop:
  for (true) {
    n, err := theInput.Read(theBuffer)
    if (nil != err) {
      if (io.EOF == err) {
        printDebug("hit end of input file")
        break
      }
      printFatal(err)
    }
    printDebug(fmt.Sprintf( "Read %d bytes at offset %d.", n, theOffset))
    i := 0
    //
    // When reading large files or block devices, n should be a power of 2
    // a lot larger than rowLength.  However, must handle arbitrary input
    // (from a tty, for example).
    //
    // Just in case we got out of alignment with integral rows:
    //
    if (0 != (theOffset & rowMask)) {
      printDebug("Need to get back into row alignment...")
      toAdjust := 0
      for (0 != ((theOffset + uint64(toAdjust)) & rowMask)) {
        toAdjust += 1
      }
      printDebug(fmt.Sprintf( "Initial toAdjust: %d", toAdjust))
      if (n < toAdjust) {
        // Don't try to go past the end of valid data.
        toAdjust = n
      }
      thisSlice := theBuffer[0:toAdjust]
      if hasNonZero(thisSlice) {
        previousOffset = printHyphensAndRow( previousOffset, theOffset,
                                             theOutput, thisSlice, printTable)
        rowsPrinted += 1
      }
      i += toAdjust
      //
      // Unless we're at the end of the buffer, verify row alignment.
      if (n > i) {
        if (0 != ((theOffset + uint64(i)) & rowMask)) {
          printError(fmt.Sprintf( "Alignment to row failed: %d, %d",
                                  theOffset, i))
          // That function does not return.
        }
      } else {
        printDebug("No full row to attempt to do.")
      }
    }
    //
    // Should be back in row alignment.
    //
    //
    // For speed, do initial checking here rather than in hasNonZero().
    //
    foundBad := false
    for j := i; n > j; j++ {
      if (0 != theBuffer[j]) {
        foundBad = true
        break
      }
    }
    if (foundBad) {
      for (n > i) {
        // Process one row at a time.
        rowEnd := i + rowLength
        if (n < rowEnd) {
          rowEnd = n
        }
        thisSlice := theBuffer[i:rowEnd]
        if (hasNonZero(thisSlice)) {
          previousOffset = printHyphensAndRow( previousOffset,
                                               theOffset + uint64(i),
                                               theOutput, thisSlice,
                                               printTable)
          rowsPrinted += 1
        }
        i = rowEnd
      }
    }
    theOffset += uint64(n)
  }
  printTheHyphens( previousOffset, theOffset, theOutput)
  //
  // Prepare to print summary statistics:
  miBytes := float32(theOffset) / 1024.0 / 1024.0
  giBytes := miBytes / 1024.0
  endTime := time.Now()
  elapsed := float32(endTime.Sub(startTime)) / 1000.0 / 1000.0 / 1000.0
  miBps := miBytes / elapsed
  stats := []string{}
  stats = append( stats, "")
  stats = append( stats, fmt.Sprintf( "     Input file: %s",   inputName))
  stats = append( stats, fmt.Sprintf( "    Output file: %s",   outputName))
  stats = append( stats, fmt.Sprintf( "Bytes processed: %d",   theOffset))
  stats = append( stats, fmt.Sprintf( "  MiB processed: %.2f", miBytes))
  stats = append( stats, fmt.Sprintf( "  GiB processed: %.2f", giBytes))
  stats = append( stats, fmt.Sprintf( "   Rows printed: %d",   rowsPrinted))
  stats = append( stats, fmt.Sprintf( "   elapsed secs: %.2f", elapsed))
  stats = append( stats, fmt.Sprintf( " MiB per second: %.2f", miBps))
  stats = append( stats, "")
  //
  // Print summary statistics:
  for _, s := range stats {
    if (os.Stdout != theOutput) {
      fmt.Fprintln( theOutput, s)
    }
    printInfo(s)
  }
  printDebug("doWork finished.")
}

//////////////////////////////////////////////////////////////////////////////

func closeFiles( theInput * os.File, theOutput * os.File) {
  printDebug("closeFiles starting:")
  if (os.Stdout != theOutput) {
    defer theOutput.Close()
  }
  if (os.Stdin != theInput) {
    defer theInput.Close()
  }
  printDebug("closeFiles finished.")
}

//////////////////////////////////////////////////////////////////////////////

// Start here:
func main() {
  inputName, outputName, theInput, theOutput := getArgs()
  doWork( inputName, outputName, theInput, theOutput, makePrintTable())
  closeFiles( theInput, theOutput)
}

//////////////////////////////////////////////////////////////////////////////

// The End.
